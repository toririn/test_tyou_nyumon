require "rspec"
require_relative "fizz_bazz"

RSpec.describe "FizzBazzクラスをテストする" do
  describe ".fizz_bazz メソッドをテストする" do
    context "(引数に3を渡すとき)" do
      it "Fizzの文字列が返ること" do
        num = 3
        expect(FizzBazz.fizz_bazz(num)).to eq "Fizz"
      end
    end

    context "(引数に5を渡すとき)" do
      it "Bazzの文字列が返ること" do
        num = 5
        expect(FizzBazz.fizz_bazz(num)).to eq "Bazz"
      end
    end
  end
end
