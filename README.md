# RSpec(テストフレームワーク)なしで紹介する自動テスト超入門

* [こちらの記事](https://qiita.com/toririn/items/7029b2a593440cbc8b54)のお試しリポジトリです

## 実行コマンド

### オリジナルテストスペックの実行方法

```
ruby fizz_bazz_test.rb
```

### `RSpec` の実行方法

* もし `RSpec` をインストールしていなければ…

```
gem install rspec
```

* `RSpec` の実行方法(日本語版)

```
rspec rspec fizz_bazz_spec.rb -f d
```

* `RSpec` の実行方法(英語版)

```
rspec rspec fizz_bazz2_spec.rb -f d
```
