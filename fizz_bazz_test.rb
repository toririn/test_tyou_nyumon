#! /usr/bin/ruby

# FizzBazzクラスを相対パスで読み込む
require_relative "fizz_bazz"

puts "FizzBazzクラスをテストする"
puts "  .fizz_bazz メソッドをテストする"
puts "    (引数に3を渡すとき)"
num1 = 3
result1 = FizzBazz.fizz_bazz(num1)
puts "      Fizzの文字列が返ること"
if result1 == "Fizz"
  puts "        【成功】"
else
  puts "        【失敗】"
  puts "        実行結果: #{result1}"
end

puts "    (引数に5を渡すとき)"
num2 = 5
puts "      Bazzの文字列が返ること"
result2 = FizzBazz.fizz_bazz(num2)
if result2 == "Bazz"
  puts "        【成功】"
else
  puts "        【失敗】"
  puts "        実行結果: #{result2}"
end
