class FizzBazz
  def self.fizz_bazz(num)
    if num % 3 == 0
      "Fizz"
    elsif num % 5 == 0
      "Bazz"
    end
  end
end
