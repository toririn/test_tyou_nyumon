require 'rspec'
require_relative "fizz_bazz"

RSpec.describe FizzBazz do
  describe ".fizz_bazz" do
    subject { FizzBazz.fizz_bazz(num) }
    context "(When num is given 3)" do
      let(:num) { 3 }
      it { is_expected.to eq "Fizz" }
    end

    context "(When num is given 5)" do
      let(:num) { 5 }
      it { is_expected.to eq "Bazz" }
    end
  end
end

